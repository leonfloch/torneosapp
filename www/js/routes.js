angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      
        
    .state('tabsController.torneos', {
      url: '/page24',
      views: {
        'tab10': {
          templateUrl: 'templates/torneos.html',
          controller: 'torneosCtrl'
        }
      }
    })
  
    .state('tabsController.equipos', {
      url: '/page25',
      views: {
        'tab10': {
          templateUrl: 'templates/equipos.html',
          controller: 'equiposCtrl'
        }
      }
    })
        
    .state('tabsController.jugadores', {
      url: '/page26',
      views: {
        'tab12': {
          templateUrl: 'templates/jugadores.html',
          controller: 'jugadoresCtrl'
        }
      }
    })

    .state('tabsController', {
      url: '/page23',
      abstract:true,
      templateUrl: 'templates/tabsController.html'
    })
        
    .state('tabsController.creaTorneo', {
      url: '/page27',
      views: {
        'tab10': {
          templateUrl: 'templates/creaTorneo.html',
          controller: 'creaTorneoCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.tabla', {
      url: '/page28',
      views: {
        'tab13': {
          templateUrl: 'templates/tabla.html',
          controller: 'tablaCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.encuentros', {
      url: '/page29',
      views: {
        'tab11': {
          templateUrl: 'templates/encuentros.html',
          controller: 'encuentrosCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.crearEquipo', {
      url: '/page30',
      views: {
        'tab10': {
          templateUrl: 'templates/crearEquipo.html',
          controller: 'crearEquipoCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.seleccionarJugadores', {
      url: '/page31',
      views: {
        'tab10': {
          templateUrl: 'templates/seleccionarJugadores.html',
          controller: 'creaTorneoCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.crearJugador', {
      url: '/page33',
      views: {
        'tab12': {
          templateUrl: 'templates/crearJugador.html',
          controller: 'crearJugadorCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.historialTorneos', {
      url: '/page34',
      views: {
        'tab10': {
          templateUrl: 'templates/historialTorneos.html',
          controller: 'historialTorneosCtrl'
        }
      }
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/page23/page24');

});
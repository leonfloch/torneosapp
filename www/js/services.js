angular.module('app.services', [])

.factory("TorneoFactory", ["$firebaseObject", "$firebaseArray", 
	function($firebaseObject, $firebaseArray){
	
	var ref = new Firebase("https://torneosapp.firebaseio.com");
	var torneoActivo = $firebaseObject(ref.child('torneoActivo'));
	var listaEquipos = "equipos";
	var listaJugadores = "jugadores";
	var listaNiveles = "niveles";
	var listaTorneos = "torneos";
	var listaEncuentros = "encuentros";
	var equipos = $firebaseArray(ref.child(listaEquipos));
	var niveles = $firebaseArray(ref.child(listaNiveles));
	var jugadores = $firebaseArray(ref.child(listaJugadores));
	var torneos = $firebaseArray(ref.child(listaTorneos));
	var encuentros = $firebaseArray(ref.child(listaEncuentros));	
	var tabla = $firebaseArray(ref.child('tabla'));
	
	
	
	
	
	
	
	
	//---------------------------------
	// METODOS PRIVADOS
	//---------------------------------
	var getTiposEquipo = function() {
		return [{valor: "CLUB"}, {valor: "SELECCION"}];
	}
	
	
	//---------------------------------
	// METODOS PUBLICOS
	//---------------------------------
	return {
		
		getTiposPartidos : function() {
			return [{valor: "SOLO UN PARTIDO"}, {valor: "IDA Y VUELTA"}];
		},
		
		getTiposEquipo: function() {
			return getTiposEquipo();	
		},
		
		getTiposEquipoTorneo: function() {
			var tipos = getTiposEquipo();
			tipos.push({valor: "CLUB Y SELECCION"});			
			return tipos;				
		},
		
		getTorneos: function() {
			return torneos;	
		},
		
		getTabla: function() {
			return tabla;	
		},
		
		getNiveles: function() {						
			return niveles;
		},
		
		getEquipos: function() {
			return equipos;	
		},
		
		getJugadores: function() {
			return jugadores;	
		},
		
		agregarJugador: function(jugador) {
			jugadores.$add({
				"nombre": jugador.nombre.toUpperCase(),
				"alias": jugador.alias.toUpperCase(),
			});		
		},
		
		agregarEquipo: function(equipo) {
			equipos.$add({
				"nombre": equipo.nombre.toUpperCase(),
				"nivel": equipo.nivel.toUpperCase(),
				"tipo": equipo.tipo.toUpperCase(),
			});		
		},
		
		/**
		 * Crea un nuevo torneo en al app		  
		 */ 
		crearTorneo: function(torneo) {	
			//deja inactivos los otros torneos		
			angular.forEach(torneos, function(value) {
				value.estado = "I"
				torneos.$save(value);				
			}); 
			
			torneos.$add({
				"nombre": torneo.nombre.toUpperCase(),
				"nivel": torneo.nivel.toUpperCase(),
				"partidos": torneo.partidos,
				"tipo": torneo.tipo.toUpperCase(),
				"estado": "A",
			});
			
			//actualiza el torneo activo
			torneoActivo.nombre = torneo.nombre.toUpperCase();
			torneoActivo.$save();
		},
		
		/**
		 * Activa el torneo enviado por parametro
		 */ 
		activarTorneo: function(torneo) {
			torneoActivo.nombre = torneo.nombre.toUpperCase();
			torneoActivo.$save();
			
			//deja inactivos los otros torneos		
			angular.forEach(torneos, function(value) {
				if (value.nombre.toUpperCase() === torneo.nombre.toUpperCase()) {
					value.estado = "A"		
				} else {
					value.estado = "I"
				} 
				torneos.$save(value);
			}); 
		},
		
		/**
		 * Crea una lista de nuevos encuentros
		 */ 
		crearEncuentros: function(encuentrosCrear) {
            //console.log(encuentrosCrear);
			angular.forEach(encuentrosCrear, function(value) {
				encuentros.$add(value);
			});
		},
		
		/**
		 * crea la tabla de posiciones para un torneo nuevo
		 */ 
		crearTabla: function(jug, torneo) {						
			angular.forEach(jug, function(j) {
				var tor = torneo.nombre + "-" + j.nombre;
				//var key = tor.replace(/[.$\[\]\/#]/, ','/);												
				ref.child('tabla').child(tor).update({
					"torneo" : torneo.nombre.toUpperCase(),
					"JUGADOR": j.nombre.toUpperCase(),
					"PJ": parseInt(0),
					"PG": parseInt(0),
					"PE": parseInt(0),
					"PP": parseInt(0),
					"GF": parseInt(0),
					"GC": parseInt(0),
					"GD": parseInt(0),
					"P": parseInt(0),
				});	
			});
		},
		
		/**
		 * Retorna los encuentros del torneo enviado
		 */ 
		getEncuentros: function() {            
			return encuentros;
		},
		
		/**
		 * Retorna el torneo activo
		 * solo puede axistir uno activo
		 */ 
		getTorneoActivo: function() {			
			return torneoActivo;
		},
		
		/**
         * Cada vez que se actualiza el marcador de un encuentro se
		 * Actualiza la tabla de posiciones basado en el 
		 * resultado del encuentro
		 */ 
		actualizarTabla: function(encuentro) {
			encuentros.$save(encuentro);
                                               
            //se utiliza para resetear la tabla de posiciones
            //ya que se vuelve a calcular recorriendo los encuentros
            angular.forEach(tabla, function(reg) {
                //if (torneoActivo.nombre === reg.torneo) {
                    reg.GF = 0;
                    reg.P = 0;
                    reg.GC = 0;
                    reg.GD = 0;
                    tabla.$save(reg);
                //}
                
            });
            
			//se recorren todos los encuentros del torneo activo, se va calculando
            //los datos para cada jugador
			angular.forEach(encuentros, function(reg) {
                
                if (torneoActivo.nombre === reg.torneo) {
                    var regGoles1 = parseInt(reg.equipo1.goles);
                    var regGoles2 = parseInt(reg.equipo2.goles);
                    
                    var idTabla = reg.torneo + "-" + reg.equipo1.nombre;
                    var regTabla = ref.child('tabla').child(idTabla);
                    var goles = 0;
                    var golesC = 0;
                    var puntos = 0;                    
                    regTabla.on("value", function(snapshot) {
                        goles = snapshot.val().GF;
                        golesC = snapshot.val().GC;
                        puntos = snapshot.val().P;                           
                    });                                                                                                   
                    var regTab = $firebaseObject(ref.child('tabla').child(idTabla));                    
                    regTab.GF = goles + regGoles1;
                    regTab.GC = golesC + regGoles2; 
                    regTab.GD = regTab.GF - regTab.GC                   
                    regTab.P = (regGoles1 > regGoles2) ? puntos + 3 : (regGoles1 === regGoles2) ? puntos + 1 : puntos;
                    regTab.$save();
                    
                    
                    
                    //se realiza el mismo proceso para el equipo2
                    var idTabla2 = reg.torneo + "-" + reg.equipo2.nombre;
                    var regTabla2 = ref.child('tabla').child(idTabla2);
                    var goles2 = 0;
                    var golesC2 = 0;
                    var puntos2 = 0;
                    regTabla2.on("value", function(snapshot) {
                        goles2 = snapshot.val().GF;
                        golesC2 = snapshot.val().GC;
                        puntos2 = snapshot.val().P;    
                    });                                        
                    var regTab2 = $firebaseObject(ref.child('tabla').child(idTabla2));                    
                    regTab2.GF = goles2 + regGoles2;
                    regTab2.GC = golesC2 + regGoles1;  
                    regTab2.GD = regTab2.GF - regTab2.GC                  
                    regTab2.P = (regGoles2 > regGoles1) ? puntos2 + 3 : (regGoles1 === regGoles2) ? puntos2 + 1 : puntos2;
                    regTab2.$save();
                    
                }
               
            });
			
		},
		
		
		
	};

}])

.factory("CrearToFactory", ["TorneoFactory", function(TorneoFactory) {
	
	
	var crearEncuentros = function(encuentros) {
		TorneoFactory.crearEncuentros(encuentros);
	}
	
	var crearTabla = function(jugadores, torneo) {
		TorneoFactory.crearTabla(jugadores, torneo);
	}	
	
	
		
	return {
		
		/**
		 * Genera los partidos aleatorios del torneo.
		 * asigna equipos a los jugadores.
		 */
		configurarPartido: function(torneo, equipos, jugadores) {
			
			var encunentros = [];
								
			//-------------------------------------------
            
			//genera los encuentros
			var jugaTmp = [];
			jugaTmp = angular.copy(jugadores);
			
			angular.forEach(jugadores, function(j) {
                //console.log(j);                               		
				var ju = j;
				jugaTmp.splice(0, 1);
				
				angular.forEach(jugaTmp, function(jd) {
					encunentros.push({
						"torneo": torneo.nombre.toUpperCase(),						
						"equipo1": {"jugador": ju, goles: 0},
						"equipo2": {"jugador": jd, goles: 0},
					});
				});
							
			});
			crearEncuentros(encunentros);
			//-------------------------------------------
						
			//-------------------------------------------
			//crea la tabla de posiciones			
			crearTabla(jugadores, torneo);
			
			//-------------------------------------------
		}
		
			
	};
}])	

.service('BlankService', [function(){

}]);


angular.module('app.controllers', [])
  
.controller('torneosCtrl', function($scope, TorneoFactory) {
	$scope.torneoActivo = TorneoFactory.getTorneoActivo();

})
   
.controller('equiposCtrl', function($scope, TorneoFactory) {
	$scope.equipos = TorneoFactory.getEquipos();
})
   
.controller('jugadoresCtrl', function($scope, TorneoFactory) {
	
	$scope.jugadores = TorneoFactory.getJugadores()

})
      
.controller('creaTorneoCtrl', function($scope, $state, TorneoFactory, CrearToFactory) {
	$scope.torneo = {};
	$scope.jugadores = TorneoFactory.getJugadores()
	$scope.niveles = TorneoFactory.getNiveles();
	$scope.equipos = TorneoFactory.getEquipos();
	$scope.tipoPartidos = TorneoFactory.getTiposPartidos();
	$scope.tipoEquipos = TorneoFactory.getTiposEquipoTorneo();
	
	
	/**
	 * Crea un nuevo torneo
	 */ 
	$scope.addTorneo = function(form) {
        //console.log(form);
		var valido = true;
		
		//valida que por lo menos existan dos equipos
		var cont = 0;
		angular.forEach($scope.jugadores, function(jugador) {
			if (jugador.select) {
				cont++;
			}
		});		
		if (cont < 2) {
			valido = false;
			console.log('Deben existir por lo menos dos jugadores');
		}
		
						        
        if(form.$valid && valido) {
            console.log('Sign-In', $scope.torneo.nombre);
			
			//equipos nivel
			var equiposNivel = [];		
			angular.forEach($scope.equipos, function(value) {
				if ($scope.torneo.nivel === value.nivel) {
					this.push(value);	
				}			
			}, equiposNivel);
			
			//jugadores seleccionados
			var jugadoresSelecc = [];
			angular.forEach($scope.jugadores, function(value) {
				if (value.select) {
					this.push(value);	
				}
			}, jugadoresSelecc); 
			
			TorneoFactory.crearTorneo($scope.torneo);
			CrearToFactory.configurarPartido($scope.torneo, equiposNivel, jugadoresSelecc);	
            
			$scope.torneo = {};
            $state.go('tabsController.encuentros');
        }
        
        
		
				
		
	}


})


   
.controller('tablaCtrl', function($scope, TorneoFactory) {
	$scope.tabla = TorneoFactory.getTabla();
	$scope.torneoActivo = TorneoFactory.getTorneoActivo();
})
   
.controller('encuentrosCtrl', function($scope, TorneoFactory) {
	
	$scope.torneoActivo = TorneoFactory.getTorneoActivo();	
	$scope.encuentros = TorneoFactory.getEncuentros();
    //console.log($scope.encuentros);
    
	
	$scope.actualizarTabla = function(encuentro) {		
		TorneoFactory.actualizarTabla(encuentro);		
	}	

})
   
.controller('crearEquipoCtrl', function($scope, $state, TorneoFactory) {
	
	$scope.tipos = TorneoFactory.getTiposEquipo();
	$scope.niveles = TorneoFactory.getNiveles();	
	
	$scope.equipo = {};	
	
	/**
	 * Agrega un nuevo equipo a la lista
	 */ 
	$scope.addEquipo = function(formulario) {
				 
		if (formulario.$valid) {
			TorneoFactory.agregarEquipo($scope.equipo);
			$scope.equipo = {};
			$state.go('tabsController.equipos');
		}
	}

})
   

/**
 * Constrolador que se encarga de crear jugadores
 */    
.controller('crearJugadorCtrl', function($scope, $state, TorneoFactory) {
	
	$scope.jugador = {};
	
	/**
	 * adiciona un nuevo jugador
	 */ 
	$scope.addJugador = function(formulario) {		
		if (formulario.$valid) {						
			TorneoFactory.agregarJugador($scope.jugador);
			$scope.jugador = {};
			$state.go('tabsController.jugadores');
		}
	}
	

})
   
.controller('historialTorneosCtrl', function($scope, TorneoFactory) {
	$scope.torneos = TorneoFactory.getTorneos();
	
	
	$scope.activaTorneo = function(torneo) {
		TorneoFactory.activarTorneo(torneo);	
	}
	

})
 